# humanstxt.org
# The humans responsible & technology colophon

# HUMANS
  * see contributors: https://gitlab.com/fediverse/fediverse.gitlab.io/-/graphs/master

# TECHNOLOGY COLOPHON
  Hexo
  Sass
  Gulp
