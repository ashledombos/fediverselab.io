
---
layout: "post"
title: "Fediverse testsuite (link update)"
date: 2018-03-15
tags:
    - fediverse
preview: "This tool runs CI tests in addition to the project's own testsuite and will help developers working on federated platforms spot bugs easier."
url: "https://testsuite.feneas.org"
lang: en
---

This tool runs CI tests in addition to the project's own testsuite, to help developers working on platforms federating with each other spot bugs easier.
More info [here](https://testsuite.feneas.org).
