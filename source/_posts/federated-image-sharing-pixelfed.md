
---
layout: "post"
title: "Federated image sharing Pixelfed"
date: 2018-06-02
tags:
    - fediverse
preview: "Pixelfed reached beta stage. It supports ActivityPub and will federate with other networks. Follow @pixelfed on mastodon.social for latest news"
url: "https://pixelfed.org"
lang: en
---

Federated image sharing platform [Pixelfed](https://pixelfed.org) reached beta stage. It supports ActivityPub protocol and intends to replace proprietary Instagram service. You can get the latest news about the project by following @pixelfed [Mastodon account](https://mastodon.social/@pixelfed).

Two other similar projects in development are [Fontina](https://github.com/beta-phenylethylamine/fontina) and [Zinat](https://github.com/yabirgb/zinat).
