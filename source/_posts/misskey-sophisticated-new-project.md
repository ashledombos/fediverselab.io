
---
layout: "post"
title: "Misskey part of Fediverse"
date: 2018-05-06
tags:
    - misskey
preview: "Welcome Misskey - ActivityPub platform. Polls, recommended users, server info, dark mode, emoji reactions! (news by @wakest@mastodon.social)"
url: "https://github.com/syuilo/misskey"
lang: en
---

Welcome [Misskey](https://github.com/syuilo/misskey) - a sophisticated microblogging platform. Polls, recommended users, server info, dark mode, emoji reactions and much more! It communicates with ActivityPub servers, so you can follow users from other compatible Fediverse platforms. Misskey is in active development. Join and contribute! (news provided by @wakest@mastodon.social).
